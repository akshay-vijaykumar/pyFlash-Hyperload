pyFlash-Hyperload
Version : 1.0

About the Tool:
A firmware flashing tool for SJOne using Python. This is based on the 'Hyperload' Protocol for flashing images at high speeds.

Required Packages
	- Python   [v]
	- pyserial [v3.0.1]
	- intelhex [v2.0]

Steps to Run:
	- Modify pyFlash-Hyperload.py with the required Filepaths and Baudrate.
	- Run "sudo python pyFlash-Hyperload.py"
